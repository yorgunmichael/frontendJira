import {importProvidersFrom} from '@angular/core';
import {AppComponent} from './app/app.component';
import {FormsModule} from '@angular/forms';
import {withInterceptorsFromDi, provideHttpClient, HTTP_INTERCEPTORS} from '@angular/common/http';
import {APP_ROUTES} from './app/app.routing';
import {BrowserModule, bootstrapApplication} from '@angular/platform-browser';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {PreloadAllModules, provideRouter, withPreloading} from "@angular/router";
import {APP_INITIALIZER} from "@angular/core";
import {KeycloakAngularModule, KeycloakBearerInterceptor, KeycloakService} from "keycloak-angular";
import {SecurityGuard} from "./app/core/guards/security.guard";
import {AuthInterceptor} from "./app/core/interceptors/auth.interceptor";
import {NzNotificationModule} from "ng-zorro-antd/notification";


export function kcFactory(kcService: KeycloakService) {
  return () => {
    kcService.init({
      config: {
        realm: "app-realm",
        clientId: "app-frontend",
        url: "http://localhost:8080"
      },

      initOptions: {
        onLoad: "check-sso",
        checkLoginIframe: true,

      }
    })
  }
}

bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(BrowserModule, FormsModule, BrowserAnimationsModule, SecurityGuard, NzNotificationModule),
    provideRouter(APP_ROUTES, withPreloading(PreloadAllModules)),
    provideHttpClient(withInterceptorsFromDi()),

    KeycloakAngularModule,
    KeycloakService,
    KeycloakBearerInterceptor,
    SecurityGuard,


    {
      provide: APP_INITIALIZER,
      useFactory: kcFactory,
      deps: [KeycloakService],
      useClass: KeycloakBearerInterceptor,
      multi: true
    },

    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ]
})
  .catch(err => console.error(err));
