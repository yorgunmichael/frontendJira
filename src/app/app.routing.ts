import {Routes} from '@angular/router';

export const APP_ROUTES: Routes = [

  {
    path: '',
    redirectTo: 'client',
    pathMatch: 'full'
  },
  {
    path: '',
    loadChildren: () => import('./features/client/dashboard/dashboard-routing'),

  },
  {
    path: 'gestion-ticket', loadChildren: () => import('./features/client/gestion-tickets/gestionTickets-routing')
  }
];
