import {Injectable} from '@angular/core';
import {BehaviorSubject, map, Observable, of, tap} from "rxjs";
import {User} from "../../user";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ProfileDto} from "../../profileDto";
import {KeycloakService} from "keycloak-angular";
import {KeycloakProfile} from "keycloak-js";

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  public userSubject = new BehaviorSubject<string>('');
  authenticatedUser: User | undefined;
  public profile?: KeycloakProfile;

  constructor(private http: HttpClient, private keycloakService: KeycloakService ) {
  }

  login(email: string, password: string): Observable<any> {
    const data = {
      email: email,
      password: password
    };
    console.log(data)
    const url = `http://localhost:8081/myapp/users/login?email=${email}&password=${password}`;
    console.log(url);
    const headers = new HttpHeaders({
      Authorization: `Bearer ${this.keycloakService.getKeycloakInstance().token}`,
    });
    console.log('headers', headers)
    // console.log('headers', this.keycloakService.getKeycloakInstance().token)
    // console.log('headers', this.keycloakService.getUserRoles())
    // console.log(this.keycloakService.getKeycloakInstance().loadUserProfile().then(
    //   profile => {
    //     this.profile = profile;
    //     console.log('profile:====> ', this.profile.firstName);
    //   }
    // ))
    // this.keycloakService.loadUserProfile()
    //   .then(profile => {
    //     this.profile = profile;
    //     console.log('profile:====> ', this.profile.firstName);
    //   });
    console.log('load');


    return this.http.post(url, data, { headers: headers }).pipe(
      map((response: any) => {
        return {
          id: response.id,
          name: response.name,
          email: response.email,
          password: response.password,
          profileDto: response.profileDto as ProfileDto
        };
      }),
      tap((response: any) => {
        console.log('Nous sommes dans le service: ' + JSON.stringify(response));
        this.userSubject.next(response.email);
      })
    );
  }

  getUserName(): Observable<string> {
    return this.userSubject.asObservable();
  }

  authenticateUser(user: User): Observable<boolean> {
    this.authenticatedUser = user;
    localStorage.setItem("authUser", JSON.stringify({username: user.name, roles: [user.profileDto.type_profile], jwt: "JWT_TOKEN"}));
    return of(true);
  }

  // hasRole(profile: string): boolean {
  //   return this.authenticatedUser!.profileDto.type_profile.includes(profile);
  // }
  //
  // isAuthenticated() {
  //   return this.authenticatedUser != undefined;
  // }

  logout(): Observable<boolean> {
    this.authenticatedUser=undefined;
    localStorage.removeItem('username');
    localStorage.removeItem('projects');
    localStorage.removeItem('selectedProject');
    localStorage.removeItem('issues');
    localStorage.removeItem("authUser");
    return of (true);
  }
}
