import {Injectable} from '@angular/core';
import {KeycloakEvent, KeycloakEventType, KeycloakService} from "keycloak-angular";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {interval} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  public profile?: any;

  constructor(public kcService: KeycloakService, private http: HttpClient) {
    this.kcService.keycloakEvents$.subscribe((event: KeycloakEvent) => {
      if (event.type === KeycloakEventType.OnAuthSuccess) {
        this.init();
      }
    });
    this.init();
  }

  init() {
    console.log('Nous sommes dans Keycloak');
    const authenticated = this.kcService.getKeycloakInstance().authenticated;
    if (authenticated) {

      const token = this.kcService.getKeycloakInstance().token;
      const headers = new HttpHeaders().set('Authorization', `Bearer ${token}`);

      this.http.get<any>('http://localhost:8080/realms/app-realm/protocol/openid-connect/userinfo', {headers})
        .subscribe(
          (userInfo) => {
            this.profile = userInfo;
            console.log('Sub :', this.profile?.sub);
            console.log('Email vérifié :', this.profile?.email_verified);
            console.log('Nom :', this.profile?.name);
            console.log('Nom d\'utilisateur préféré :', this.profile?.preferred_username);
            console.log('Prénom :', this.profile?.given_name);
          },
          (error) => {
            console.error('Une erreur s\'est produite lors de la récupération des informations de l\'utilisateur :', error);
          }
        );

      const refreshInterval = 300;
      const tokenRefreshSubscription = interval(refreshInterval * 1000)
        .subscribe(() => {
          this.kcService.updateToken(refreshInterval)
            .then((refreshed) => {
              if (refreshed) {
                console.log('Token rafraîchi avec succès');
              } else {
                console.warn('Le rafraîchissement du token a échoué');
              }
            })
            .catch((error) => {
              console.error('Une erreur s\'est produite lors du rafraîchissement du token:', error);
            });
        });
    }
  }


  hasRoleIn(roles: string[]): boolean {
    let userRoles = this.kcService.getUserRoles();
    for (let role of roles) {
      if (userRoles.includes(role))
        return true;
    }
    return false;
  }

}
