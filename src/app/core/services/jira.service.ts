import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable, of, tap, throwError} from "rxjs";
import {Project} from "../../Project";
import {Issue} from "../../Issue";
import {ValidationErrors} from "@angular/forms";
import {BasicProject} from "../../BasicProject";

@Injectable({
  providedIn: 'root'
})
export class JiraService {
  private baseUrl = 'http://localhost:8081/myapp';
  issues: Issue[] = [];
  issueJira: any[] = [];


  constructor(private http: HttpClient) {
  }

  getProjectsByEmail(email: string): Observable<Project[]> {
    console.log("nous somme dans le serviceeeeeeeee");
    console.log('Email envoyé à project: ', email);
    const url = `${this.baseUrl}/project?email=${email}`;

    return this.http.get<Project[]>(url).pipe(
      tap((response: any) => {
        console.log('Voici les projects', response)
      })
    );
  }


  getAllProjectsByUser(userEmail: string): Observable<BasicProject[]> {
    console.log('LE NOM DU PROJECT #####==> ', userEmail)
    const url = `${this.baseUrl}/request/jira/projects?userEmail=${userEmail}`;
    console.log(url)

    return this.http.get<BasicProject[]>(url).pipe(
      tap((listProjectJira: any) => {
        console.log('Voici la liste des projects de jira ====>= ', listProjectJira)
      })
    );
  }

  createTicket(title: string, description: string, types: string, priority: string, date_create: Date = new Date(), status: string, picture: File, url: string, name_project: string, env: string): Observable<any> {

    const formData = new FormData();
    formData.append('title', title);
    formData.append('description', description);
    formData.append('types', types);
    formData.append('priority', priority);
    formData.append('date_create', date_create.toString());
    formData.append('status', status);
    formData.append('picture', picture);
    formData.append('url', url);
    formData.append('name_project', name_project);
    formData.append('env', env);
    console.log('pppppppppp======+=======>', picture.name)

    const urls = `${this.baseUrl}/addIssue`;

    return this.http.post<any>(urls, formData).pipe(
      map((response: any) => {
        console.log("Informations soumises au backend");
        console.log(response);
        return response;
      })
    );
  }


  createIssue(issueData: any, type: string, picture: File[]): Observable<string> {
    const formData = this.setupFormData(issueData, type);
    if (picture && picture.length > 0) {
      for (let i = 0; i < picture.length; i++) {
        formData.append('pictures', picture[i]);
      }
      console.log('Pictures ====>', picture);
    }

    const urls = `${this.baseUrl}/request/jira/createIssue`;
    console.log('Voici les info envoyées à jira:', issueData);
    console.log('Voici les info envoyées à jira:', JSON.stringify(issueData));
    return this.http.post<string>(urls, formData)
  }


  getIssueChangelog(issueKey: string): Observable<any[]> {
    const url = `${this.baseUrl}/request/jira/history/${issueKey}`;
    return this.http.get<any[]>(url);
  }


  updateIssue(issueId: number, title: string, description: string, types: string, priority: string, date_create: Date = new Date(), status: string, picture: File, url: string, name_project: string, env: string): Observable<any> {
    const formData = new FormData();

    formData.append('title', title);
    formData.append('description', description);
    formData.append('types', types);
    formData.append('priority', priority);
    formData.append('date_create', date_create.toString());
    formData.append('status', status);
    formData.append('picture', picture);
    formData.append('url', url);
    formData.append('name_project', name_project);
    formData.append('env', env);

    console.log('picture=====>', picture.name)

    const urls = `${this.baseUrl}/${issueId}`;
    return this.http.put<any>(urls, formData).pipe(
      map((response: any) => {
        console.log("Informations soumises au backend pour la mise à jour");
        console.log('Mise à jour du ticket: ', response);
      })
    )
  }

  updateJiraIssue(issueKey: string, dataIssueJira: any, type: string, picture: File[]) {
    const formData = this.setupFormData(dataIssueJira, type);

    if (picture && picture.length > 0) {
      for (let i = 0; i < picture.length; i++) {
        formData.append('pictures', picture[i]);
      }
      console.log('Pictures ====>', picture);
    }

    const urls = `${this.baseUrl}/request/jira/update/${issueKey}`;
    return this.http.put<any>(urls, formData).pipe(
      map((response: any) => {
        console.log('Mise à jour du ticket Jira: ', response);
      })
    )
  }


  getJiraTickets(projectKey: string, pageSize: number, pageNumber: number): Observable<any> {
    console.log('pageSize dans service: ', pageSize)
    console.log('pageNumber dans service: ', pageNumber)
    console.log('projectKey dans le service: ', projectKey)

    const urls = `${this.baseUrl}/request/jira/listOfTicket?projectKey=${projectKey}&pageSize=${pageSize}&pageNumber=${pageNumber}`;
    console.log(urls)

    return this.http.get<any>(urls).pipe(
      map((response: any[]) => {
        console.log('RESPONSE JIRA BACKEND', response);
        this.issueJira = response;
        return response;
      })
    );
  }


  AllJiraTickets(projectKey: string, selectedTypeIssue: string): Observable<any> {
    console.log('projectKey dans le service oui: ', projectKey)

    const urls = `${this.baseUrl}/request/jira/listOfTickets?projectKey=${projectKey}`;
    console.log(urls)

    return this.http.get<any>(urls).pipe(
      map((response: any[]) => {
        const issueKeys = response
          .filter(ticket => ticket.issueType === selectedTypeIssue)
          .map(ticket => ticket.issueKey);
        console.log('Filtered issueKeys:', issueKeys);
        return issueKeys;
      })
    );
  }


  listTicket(nameProject: string, pageNumber: number, pageSize: number): Observable<any> {
    const url = `${this.baseUrl}/valueProject?nameProject=${nameProject}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    return this.http.get<any>(url).pipe(
      map((response: any) => {
        console.log('===================+>>>>+++>>>>>> ', response)
        const content: Array<any> = response.content.map((item: any) => {

          const itemProperties = Object.keys(item);
          console.log('itemmmmmmmmmm=====> ', itemProperties);

          const issue: Issue = {
            issue_id: item.issue_id,
            title: item.title,
            nameProject: item.projectName,
            description: item.description,
            typesIssue: item.issueType,
            priority: item.priority,
            dateCreated: item.date_creation,
            status: item.status,
            picture: item.picture,
            url: item.url,
            env: item.env
          };
          return issue;
        });

        this.issues = content;
        console.log('content:::=> ', content);

        return response
      })
    );
  }


  getIssue(id: number): Observable<Issue> {
    let issue = this.issues.find(issue => issue.issue_id == id);
    if (issue == undefined) return throwError(() => new Error("Issue not found"));
    console.log('METHODE GETISSUE : ', issue)
    return of(issue);
  }


  getIssueJira(issueId: string): Observable<any> {
    let issue = this.issueJira.find(issue => issue.issueKey == issueId);
    if (!issue) {
      console.log('Issue not found with issueId: ', issueId);
      return throwError(() => new Error("Issue not found with issueId: " + issueId));
    }
    console.log('Dans le service je veux issue ==> ', issue);
    return of(issue);
  }


  deleteIssueById(issueId: number) {
    const url = `${this.baseUrl}/delete/${issueId}`;
    return this.http.delete(url);
  }


  deleteIssue(issueKey: string) {
    const url = `${this.baseUrl}/request/jira/delete?issueKey=${issueKey}`;
    return this.http.delete<void>(url);
  }


  getErrorMessage(fieldName: string, errors: ValidationErrors) {
    if (errors['required']) {
      return fieldName + " est obligatoire";
    } else
      return "";
  }

  searchIssues(keyword: string, nameProject: string, pageNumber: number, pageSize: number): Observable<Issue[]> {
    const url = `${this.baseUrl}/searchIssue?nameProject=${nameProject}&pageNumber=${pageNumber}&pageSize=${pageSize}&keyword=${keyword}`;
    return this.http.get<any>(url).pipe(
      map((response: any) => {
        console.log('===================+>>>>+++>>>>>> ', response)
        const content: Array<any> = response.content.map((item: any) => {

          const itemProperties = Object.keys(item);
          console.log('Les items =====> ', itemProperties);

          const issue: Issue = {
            issue_id: item.issue_id,
            title: item.title,
            nameProject: item.projectName,
            description: item.description,
            typesIssue: item.issueType,
            priority: item.priority,
            dateCreated: item.date_creation,
            status: item.status,
            picture: item.picture,
            url: item.url,
            env: item.env
          };
          return issue;
        });

        this.issues = content;
        console.log('content:::=> ', content)
        return response
      })
    );
  }

  filterIssue(nameProject: string, statuspos1: string, statuspos2: string, pageNumber: number, pageSize: number): Observable<Issue[]> {
    const url = `${this.baseUrl}/filterIssue?nameProject=${nameProject}&statuspos1=${statuspos1}&statuspos2=${statuspos2}&pageNumber=${pageNumber}&pageSize=${pageSize}`;
    return this.http.get<any>(url).pipe(
      map((response: any) => {
        console.log('Nous sommes dans le méthode FILTER ISSUE ', response)
        const content: Array<any> = response.content.map((item: any) => {

          const itemProperties = Object.keys(item);
          console.log('Les items de FILTER DANS LE SERVICE =====> ', itemProperties);

          const issue: Issue = {
            issue_id: item.issue_id,
            title: item.title,
            nameProject: item.projectName,
            description: item.description,
            typesIssue: item.issueType,
            priority: item.priority,
            dateCreated: item.date_creation,
            status: item.status,
            picture: item.picture,
            url: item.url,
            env: item.env
          };
          return issue;
        });

        this.issues = content;
        console.log('content:::FILTER CONTENU DU CONTENT=> ', content)
        return response
      })
    );
  }


  setupFormData(issueData: any, type: string) {
    const formData = new FormData();
    formData.append('type', type);
    formData.append('projectKey', issueData.projectKey);
    formData.append('reporter', issueData.reporter);
    formData.append('issueType', issueData.issueType);
    formData.append('managerProduct', issueData.managerProduct);
    formData.append('managerRD', issueData.managerRD);
    formData.append('collaborator', issueData.collaborator);
    formData.append('environmentUrl', issueData.environmentUrl);
    formData.append('summary', issueData.summary);
    formData.append('description', issueData.description);
    formData.append('issueLinked', issueData.issueLinked);
    formData.append('commentRecipeKO', issueData.commentRecipeKO);
    formData.append('dateOfRequest', issueData.dateOfRequest);
    formData.append('deadlineIntern', issueData.deadlineIntern);
    formData.append('deadlineCustomer', issueData.deadlineCustomer);
    formData.append('dateOfRealization', issueData.dateOfRealization);
    formData.append('dateOfDelivery', issueData.dateOfDelivery);
    formData.append('comments', issueData.comments);
    formData.append('interestsCollateral', issueData.interestsCollateral);

    if (type === 'Bugs') {
      formData.append('level', issueData.level);
      formData.append('requestFormat', issueData.requestFormat);
      formData.append('detectedBy', issueData.detectedBy);
    } else if (type === 'Deployment') {
      formData.append('priority', issueData.priority);
      formData.append('nature', issueData.nature);
      formData.append('recognitionEvolution', issueData.recognitionEvolution);
      formData.append('estimatePure', issueData.estimatePure);
      formData.append('estimateTotal', issueData.estimateTotal);
    } else if (type === 'Evolution') {
      formData.append('priority', issueData.priority);
      formData.append('nature', issueData.nature);
      formData.append('estimatePure', issueData.estimatePure);
      formData.append('estimateTotal', issueData.estimateTotal);
      formData.append('recognitionEvolution', issueData.recognitionEvolution);
      formData.append('requestFormat', issueData.requestFormat);
    }

    formData.append('issues', JSON.stringify(issueData));

    return formData;
  }

}
