import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {SecurityService} from "../services/security.service";
import {KeycloakService} from "keycloak-angular";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private securityService: SecurityService, private keycloakService: KeycloakService) {
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const requiredRoles = route.data['roles'];


    if(!this.keycloakService.getKeycloakInstance().authenticated) {
      this.keycloakService.login({
        redirectUri: window.location.origin
      });
      return false;
    }


    if (!requiredRoles || requiredRoles.length === 0){
      return true
    }

    if(this.securityService.hasRoleIn(requiredRoles)) {
      return true;
    }

    this.router.navigateByUrl('/client/errorpage');
    return false;
  }

}
