import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {KeycloakAuthGuard, KeycloakService} from 'keycloak-angular';

@Injectable({
  providedIn: 'root'
})
export class SecurityGuard extends KeycloakAuthGuard {
  constructor(
    protected override router: Router,
    protected readonly keycloakService: KeycloakService) {
    super(router, keycloakService);
  }

  async isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise(async (resolve, reject) => {
      if (!this.keycloakService.getKeycloakInstance().authenticated) {
        await this.keycloakService.login({
          redirectUri: window.location.origin
        });
      }
      console.log(window.location.origin)

      const requiredRoles = route.data['roles'];
      if (!(requiredRoles instanceof Array) || requiredRoles.length === 0) {
        return true
      }
       const hasRole = requiredRoles.every((role) => this.roles.includes(role));
      console.log('Utilisateur authentifié: ', hasRole)
      console.log('Rôles extraits:', requiredRoles);
      console.log('Rôles de l\'utilisateur authentifié:', this.roles);

      return hasRole;
    });
  }
}
