import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse, HttpErrorResponse
} from '@angular/common/http';
import {catchError, Observable, tap, throwError} from 'rxjs';
import {KeycloakService} from "keycloak-angular";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private serviceKeycloak: KeycloakService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('Request interceptor .......')
    const token = this.serviceKeycloak.getKeycloakInstance().token;
    if (token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
    }

    return next.handle(request).pipe(
      tap((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log('Requête sortante:', request);
        }
      }),
      catchError((error: HttpErrorResponse) => {
        if (error.status === 401) {
          this.serviceKeycloak.getKeycloakInstance().logout({
            redirectUri: window.location.origin
          });
          console.log('Reconnection')
        }
        return throwError(error);
      })
    );

  }

}

