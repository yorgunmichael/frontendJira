import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzBreadCrumbModule} from "ng-zorro-antd/breadcrumb";
import {NzMenuModule} from "ng-zorro-antd/menu";
import {Router, RouterLink, RouterModule} from "@angular/router";
import {ListTicketsComponent} from "../../features/client/gestion-tickets/list-tickets/list-tickets.component";
import {FormTicketComponent} from "../../features/client/gestion-tickets/form-ticket/form-ticket.component";
import {
  FormConfirmationComponent
} from "../../features/client/gestion-tickets/form-confirmation/form-confirmation.component";
import {SecurityService} from "../../core/services/security.service";


@Component({
  selector: 'app-main-layout',
  standalone: true,
  imports: [CommonModule, NzLayoutModule, NzIconModule, NzBreadCrumbModule, NzMenuModule, RouterLink, RouterModule, ListTicketsComponent, FormTicketComponent, FormConfirmationComponent],
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.css']
})
export class MainLayoutComponent {
  isCollapsed = false;

  constructor(public securityService: SecurityService) {
  }

  handleLogout() {
    this.securityService.kcService.logout(window.location.origin);
  }

  // async login() {
  //   await this.securityService.kcService.login({
  //     redirectUri: window.location.origin
  //   })
  // }
}
