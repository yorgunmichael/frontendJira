import {Component} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {AuthComponent} from "./features/client/dashboard/auth/auth.component";
import {MainLayoutComponent} from "./layout/main-layout/main-layout.component";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    standalone: true,
  imports: [RouterOutlet, AuthComponent, MainLayoutComponent]
})
export class AppComponent  {
  title = 'Mon application';

}
