export interface BasicProject {
  self: string;
  key: string;
  id: number;
  name: string;
}
