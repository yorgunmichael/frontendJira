export interface ProfileDto {
  profile_id: number;
  type_profile: string;
  description: string;
}
