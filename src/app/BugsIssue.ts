export interface BugsIssue {
  type: string,
  projectKey: string,
  reporter: string,
  issueType: string,
  managerProduct: string,
  managerRD: string,
  collaborator: string,
  environmentUrl: string,
  summary: string,
  description: string,
  issueLinked: string,
  commentRecipeKO: string,
  dateOfRequest: string,
  deadlineIntern: string,
  deadlineCustomer: string,
  dateOfRealization: string,
  dateOfDelivery: string,
  comments: string,
  interestsCollateral: string,
  level: string,
  requestFormat: string,
  detectedBy: string,
  pictures: File
}



