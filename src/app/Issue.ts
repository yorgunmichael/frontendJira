export interface Issue {
  issue_id: number
  title: string;
  description: string;
  status: string;
  typesIssue: string;
  priority: string;
  dateCreated: Date;
  picture: File;
  url: string;
  nameProject: string;
  env: string
}

