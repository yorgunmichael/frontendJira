export interface IssueJira {
  summary: string;
  description: string;
  key: string;
}
