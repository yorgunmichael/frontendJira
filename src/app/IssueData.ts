export interface IssueData {
  fields: {
    project: { key: string },
    summary: string,
    description: string,
    issuetype: { id: number }
  };
}
