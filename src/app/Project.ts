export interface Project {
  project_id: number;
  project_name: string;
  user_id: number;
}
