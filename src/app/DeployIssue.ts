export interface DeployIssue {

  type: string,
  projectKey: string,
  reporter: string,
  issueType: string,
  managerProduct: string,
  managerRD: string,
  collaborator: string,
  environmentUrl: string,
  summary: string,
  description: string,
  issueLinked: string,
  commentRecipeKO: string,
  dateOfRequest: string,
  deadlineIntern: string,
  deadlineCustomer: string,
  dateOfRealization: string,
  dateOfDelivery: string,
  comments: string,
  interestsCollateral: string,
  priority: string,
  detectedBy: string,
  nature: string,
  recognitionEvolution: string,
  estimatePure: number,
  estimateTotal: number,
  pictures: File

}




