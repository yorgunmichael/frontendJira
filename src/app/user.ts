import {ProfileDto} from "./profileDto";

export interface User {
  id: number;
  name: string;
  email: string;
  password: string;
  profileDto: ProfileDto;
}
