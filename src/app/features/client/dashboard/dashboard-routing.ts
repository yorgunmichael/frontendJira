import {Route} from "@angular/router";
import {FormTicketComponent} from "../gestion-tickets/form-ticket/form-ticket.component";
import {FormConfirmationComponent} from "../gestion-tickets/form-confirmation/form-confirmation.component";
import {ListTicketsComponent} from "../gestion-tickets/list-tickets/list-tickets.component";
import {EditTicketComponent} from "../gestion-tickets/edit-ticket/edit-ticket.component";
import {AuthGuard} from "../../../core/guards/auth.guard";
import {ErrorPageComponent} from "./errorPage/error-page/error-page.component";
import {FormsJiraticketComponent} from "../gestion-tickets/forms-jiraticket/forms-jiraticket.component";
import {ListJiraticketComponent} from "../gestion-tickets/list-jiraticket/list-jiraticket.component";
import {EditJiraticketComponent} from "../gestion-tickets/edit-jiraticket/edit-jiraticket.component";
import {HistoryJiraticketComponent} from "../gestion-tickets/history-jiraticket/history-jiraticket.component";


export default [
  {
    path: 'client',
    loadComponent: () => import('./welcome-view/welcome-view.component').then(m => m.WelcomeViewComponent),
    canActivate: [AuthGuard], data: {roles: ['admin', 'user']},

    children: [
      {
        path: 'creatNewTicket',
        component: FormTicketComponent,
        canActivate: [AuthGuard], data: {roles: ['admin', 'user']},
      },

      {
        path: 'createJiraTicket',
        component: FormsJiraticketComponent,
        canActivate: [AuthGuard], data: {roles: ['admin', 'user']},
      },


      {
        path: 'formConfirmation',
        component: FormConfirmationComponent
      },

      {
        path: 'listTickets',
        component: ListTicketsComponent
      },

      {
        path: 'listTicketsJira',
        component: ListJiraticketComponent
      },


      {
        path: 'editTicket/:issueId',
        component: EditTicketComponent,
        canActivate: [AuthGuard], data: {roles: ['admin', 'user']},
      },

      {
        path: 'editTicketJira/:issueKey',
        component: EditJiraticketComponent,
        canActivate: [AuthGuard], data: {roles: ['admin', 'user']},
      },

      {
        path: 'errorpage',
        component: ErrorPageComponent,
      },

      {
        path: 'historyJira',
        component: HistoryJiraticketComponent,
      }
    ]
  },

] as Route[];
