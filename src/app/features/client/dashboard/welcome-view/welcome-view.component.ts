import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MainLayoutComponent} from "../../../../layout/main-layout/main-layout.component";
import {RouterModule} from "@angular/router";

@Component({
  selector: 'app-welcome-view',
  standalone: true,
  imports: [CommonModule, MainLayoutComponent, RouterModule],
  templateUrl: './welcome-view.component.html',
  styleUrls: ['./welcome-view.component.css']
})
export class WelcomeViewComponent {

  constructor() {
  }

}
