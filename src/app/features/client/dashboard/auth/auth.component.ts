import {Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzCheckboxModule} from "ng-zorro-antd/checkbox";
import {FormBuilder, FormGroup, FormsModule, NgForm, ReactiveFormsModule, Validators} from "@angular/forms";
import {NzInputModule} from "ng-zorro-antd/input";
import {Router, RouterModule} from "@angular/router";
import {FormTicketComponent} from "../../gestion-tickets/form-ticket/form-ticket.component";
import {FormConfirmationComponent} from "../../gestion-tickets/form-confirmation/form-confirmation.component";
import {AuthentificationService} from "../../../../core/services/authentification.service";
import {User} from "../../../../user";

@Component({
  selector: 'app-auth',
  standalone: true,
  imports: [CommonModule, NzButtonModule, NzFormModule, NzCheckboxModule, ReactiveFormsModule, NzInputModule, FormsModule, RouterModule, FormTicketComponent, FormConfirmationComponent],
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  errorMessage: any;
  credentialsForm!: FormGroup;


  constructor(private fb: FormBuilder, private router: Router, private authService: AuthentificationService) {

  }

  ngOnInit(): void {
    this.credentialsForm = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      remember: [false]
    });
  }

  handlelogin() {
    let email = this.credentialsForm.value.email;
    let password = this.credentialsForm.value.password;

    console.log('Email:', email);
    console.log('Password:', password);
    this.authService.login(email, password).subscribe({
      next: (user: User) => {
        this.authService.authenticateUser(user).subscribe({
          next: (data: boolean) => {
            console.log('data =====> ', data)
            console.log('User =====> ', user)
            this.router.navigateByUrl('/client');
            console.log('Après la page client', )
          }
        })
      },
      error: (err) => {
        this.errorMessage = 'Email ou mot de passe invalide.';
      }
    })
  }

}





