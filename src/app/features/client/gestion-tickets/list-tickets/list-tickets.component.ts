import {Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainLayoutComponent} from "../../../../layout/main-layout/main-layout.component";
import {NzSelectModule} from "ng-zorro-antd/select";
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {Router, RouterModule} from "@angular/router";
import {JiraService} from "../../../../core/services/jira.service";
import {Project} from "../../../../Project";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzIconModule} from "ng-zorro-antd/icon";
import {Issue} from "../../../../Issue";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzBadgeModule} from "ng-zorro-antd/badge";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzTreeSelectModule} from "ng-zorro-antd/tree-select";
import {SecurityService} from "../../../../core/services/security.service";
import {NzNotificationService} from "ng-zorro-antd/notification";


@Component({
  selector: 'app-list-tickets',
  standalone: true,
  imports: [CommonModule, MainLayoutComponent, NzSelectModule, FormsModule, RouterModule, NzTableModule, NzIconModule, NzPaginationModule, NzBadgeModule, NzInputModule, NzButtonModule, NzLayoutModule, ReactiveFormsModule, NzTreeSelectModule],
  templateUrl: './list-tickets.component.html',
  styleUrls: ['./list-tickets.component.css']
})
export class ListTicketsComponent implements OnInit {

  projects: Project[] = [];
  selectedProject: Project | undefined;
  selectedStatus: string[] = [];
  name_project: string = '';
  issues: Issue[] = [];
  pageSize: number = 10;
  pageNumber: number = 1;
  totalItems: number = 11;
  searchFormGroup!: FormGroup;


  constructor(private jiraService: JiraService, private router: Router, private fb: FormBuilder, public securityService: SecurityService, private notification: NzNotificationService) {

  }

  ngOnInit(): void {

    this.searchFormGroup = this.fb.group({
      keyword: this.fb.control(null)
    });

    const storedProjects = localStorage.getItem('projects');

    if (storedProjects) {
      this.projects = JSON.parse(storedProjects);
    }

    const userEmail = this.getUserEmail();
    console.log('email de user ===>', userEmail);

    if (userEmail) {
      this.getNameProject();
    }
    this.fetchIssues();
  }

  taskStatus: any[] = [
    {
      title: 'A Faire',
      key: 'A Faire',
      value: 'A Faire',
      isLeaf: true
    },
    {
      title: 'Ouvert',
      key: 'Ouvert',
      value: 'Ouvert',
      isLeaf: true
    },
    {
      title: 'En cours',
      key: 'En cours',
      value: 'En cours',
      isLeaf: true
    },
    {
      title: 'Résolu',
      key: 'Résolu',
      value: 'Résolu',
      isLeaf: true
    },
    {
      title: 'Fermé',
      key: 'Fermé',
      value: 'Fermé',
      isLeaf: true
    }
  ];

  onProjectChange() {
    if (this.selectedProject) {
      this.name_project = JSON.parse(JSON.stringify(this.selectedProject));
      console.log("projet====>:", this.name_project);
      console.log('fin de changement de nameproject')
      this.pageNumber = 1;
      this.fetchIssues();
      localStorage.setItem('selectedProject', JSON.stringify(this.selectedProject));
    }
  }


  getUserEmail(): string | undefined {
    const userEmail = this.securityService.profile?.email;
    console.log('emmmammamail ==>', userEmail);
    return userEmail;
  }

  getNameProject() {
    const userEmail = this.getUserEmail();

    if (userEmail) {
      this.jiraService.getProjectsByEmail(userEmail).subscribe(
        namesProject => {
          this.projects = namesProject;
          console.log('data ::::', namesProject);
          localStorage.setItem('projects', JSON.stringify(this.projects));
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  onPageIndexChange(pageNumber: number) {
    console.log('changement de page: ', pageNumber);
    this.pageNumber = pageNumber;
    if (this.selectedProject) {
      this.name_project = JSON.parse(JSON.stringify(this.selectedProject));
      console.log('project actuel ======> ', this.name_project)
      this.fetchIssues();
    }
  }

  fetchIssues() {
    console.log('Obtenir la liste des tickets');
    if (this.searchFormGroup.get('keyword')?.value) {
      console.log('Obtenir la liste des tickets pour la recherche');
      this.handleSearchIssues();
    } else if (this.selectedStatus.length > 0) {
      console.log('Obtenir la liste des tickets pour les status sélectionnés');
      this.handleIssueByStatut();
    } else {
      console.log('Obtenir la liste des tickets pour aucun choix');
      this.getListIssueByProject(this.name_project);
    }
  }

  getListIssueByProject(nameProject: string) {

    if (!nameProject) {
      console.log("Aucun projet sélectionné.");
      return;
    }

    console.log('entrer de la méthode pour obtenir la liste des tickets')
    console.log('pagesize ======>', this.pageSize)
    this.jiraService.listTicket(nameProject, this.pageNumber, this.pageSize).subscribe(
      (response: any) => {
        const listIssue: Issue[] = response.content;
        this.issues = listIssue;
        console.log('Liste des tickets :', listIssue);

        this.pageSize = response.size;
        this.pageNumber = response.number;
        this.totalItems = response.totalElements;

        console.log('response content dans la méthode getListByProject ', response.content)
        console.log('pageSize ', this.pageSize)
        console.log('pageNumber ', this.pageNumber)
        console.log('totalItems ', this.totalItems)

        localStorage.setItem('projects', JSON.stringify(this.projects));
        localStorage.setItem('selectedProject', JSON.stringify(this.selectedProject));
        localStorage.setItem('issues', JSON.stringify(this.issues));
      }
    )
  }


  deleteIssue(issueId: number) {
    this.jiraService.deleteIssueById(issueId).subscribe(
      () => {
        console.log('Issue deleted succesfully');
        this.issues = this.issues.filter(issue => issue.issue_id !== issueId)
        this.createNotification('success', 'Suppression réussie', 'Le ticket a été supprimé avec succès.');
      }
    )
  }

  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }


  editIssue(issue: Issue) {
    this.router.navigateByUrl("/client/editTicket/" + issue.issue_id);
  }

  getBadgeStatus(status: string): string {
    switch (status) {
      case 'A Faire':
        return 'success';
      case 'Ouvert':
        return 'error';
      case 'En cours':
        return 'processing';
      case 'Résolu':
        return 'warning';
      case 'Fermé':
        return 'error';
      default:
        return 'default';
    }
  }

  handleSearchIssues() {
    let nameProject = this.name_project;

    const keyword = this.searchFormGroup.get('keyword')?.value;
    if (keyword && keyword.trim() !== '') {
      console.log('keyword', keyword)
      console.log('nameProject', nameProject)
      console.log('pageNumber', this.pageNumber)
      console.log('pageSize', this.pageSize)
      this.jiraService.searchIssues(keyword, nameProject, this.pageNumber, this.pageSize).subscribe(
        (response: any) => {
          const issues: Issue[] = response.content;
          this.issues = issues;
          console.log('Résultat de la recherche des tickets :', issues);
          this.pageSize = response.size;
          this.pageNumber = response.number;
          this.totalItems = response.totalElements;

          console.log(this.pageSize)
          console.log(this.pageNumber)
          console.log(this.totalItems)
        }
      );
    } else {
      this.getListIssueByProject(this.name_project);
    }
  }

  handleIssueByStatut() {
    const nameProject = this.name_project;
    const pageNumber = this.pageNumber;
    const pageSize = this.pageSize;

    if (this.selectedStatus.length > 0) {
      const statuspos1 = this.selectedStatus[0];
      const statuspos2 = this.selectedStatus.length > 1 ? this.selectedStatus[1] : '';

      this.jiraService.filterIssue(nameProject, statuspos1, statuspos2, pageNumber, pageSize).subscribe(
        (response: any) => {
          const issues: Issue[] = response.content;
          this.issues = issues;
          console.log('Résultat de la recherche des tickets par statut:', issues);
          this.pageSize = response.size;
          this.pageNumber = response.number;
          this.totalItems = response.totalElements;

          console.log(this.pageSize);
          console.log(this.pageNumber);
          console.log(this.totalItems);
        }
      );
    }
  }

  handleStatusSelection() {
    if (this.selectedStatus.length > 2) {
      this.selectedStatus = this.selectedStatus.slice(0, 2);
    } else if (this.selectedStatus.length === 0) {
      this.getListIssueByProject(this.name_project);
    } else {
      this.handleIssueByStatut();
    }
  }
}



