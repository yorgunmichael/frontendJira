import {Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {JiraService} from "../../../../core/services/jira.service";
import {Router, RouterModule} from "@angular/router";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SecurityService} from "../../../../core/services/security.service";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {Project} from "../../../../Project";
import {BasicProject} from "../../../../BasicProject";
import {NzBadgeModule} from "ng-zorro-antd/badge";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzPaginationModule} from "ng-zorro-antd/pagination";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzTableModule} from "ng-zorro-antd/table";
import {NzTreeSelectModule} from "ng-zorro-antd/tree-select";
import {MainLayoutComponent} from "../../../../layout/main-layout/main-layout.component";

@Component({
  selector: 'app-list-jiraticket',
  standalone: true,
  imports: [CommonModule, MainLayoutComponent, NzSelectModule, FormsModule, RouterModule, NzTableModule, NzIconModule, NzPaginationModule, NzBadgeModule, NzInputModule, NzButtonModule, NzLayoutModule, ReactiveFormsModule, NzTreeSelectModule],
  templateUrl: './list-jiraticket.component.html',
  styleUrls: ['./list-jiraticket.component.css']
})
export class ListJiraticketComponent implements OnInit {

  userProjects: BasicProject[] = [];
  selectedProject: Project | undefined;
  name_project: string = '';
  issues: any[] = [];
  pageSize: number = 5;
  pageNumber: number = 1;
  totalItems: number = 53;
  constructor(private jiraService: JiraService, private router: Router, private fb: FormBuilder, public securityService: SecurityService, private notification: NzNotificationService) {
  }

  ngOnInit(): void {
    const storedProjects = localStorage.getItem('projects');

    if (storedProjects) {
      this.userProjects = JSON.parse(storedProjects);
    }

    const userName = this.getUserName();
    console.log('email de user ===>', userName);

    if (userName) {
      this.getProjectsForUser();
    }
    this.fetchIssues();
  }


  getUserName(): string | undefined {
    const userName = this.securityService.profile?.name
    console.log('Nouveau nom de l utilisateur ==> ', userName);
    return userName;
  }

  getProjectsForUser() {
    const userName = this.getUserName();
    console.log('userName==>', userName)

    if (userName) {
      this.jiraService.getAllProjectsByUser(userName).subscribe(
        namesProject => {
          this.userProjects = namesProject;
          console.log('affichage des noms de projets en cours....')
          console.log('data ::::&&&&', namesProject);
          localStorage.setItem('projects', JSON.stringify(this.userProjects));
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  onPageIndexChange(pageNumber: number) {
    console.log('changement de page ==>: ', pageNumber);
    this.pageNumber = pageNumber;
    if (this.selectedProject) {
      this.name_project = JSON.parse(JSON.stringify(this.selectedProject));
      console.log('project actuel ======> ', this.name_project)
      this.fetchIssues();
    }
  }

  onProjectChange() {
    if (this.selectedProject) {
      this.name_project = JSON.parse(JSON.stringify(this.selectedProject));
      localStorage.setItem('selectedProject', JSON.stringify(this.selectedProject));
      console.log('project actuel ======> ', this.name_project)
      this.fetchIssues();
    }
  }

  fetchIssues() {
    console.log('Obtenir la liste des tickets pour aucun choix');
    this.getIssueJiraByProject(this.name_project);
  }

  getIssueJiraByProject(nameProjectJira: string) {
    if (!nameProjectJira) {
      console.log("Aucun projet JIRA sélectionné.");
      return;
    }
    console.log('entrer de la méthode pour obtenir la liste des tickets')
    console.log('pageSize dans component: ', this.pageSize)
    console.log('pageNumber dans component: ', this.pageNumber)
    console.log('totalItems dans component: ', this.totalItems)
    console.log('le nom du projet ======>', nameProjectJira)
    this.jiraService.getJiraTickets(nameProjectJira, this.pageSize, this.pageNumber).subscribe(
      (response: any[]) => {
        const listIssue: any[] = response;
        this.issues = listIssue;
        console.log('Liste des tickets :', listIssue);


        localStorage.setItem('projects', JSON.stringify(this.userProjects));
        localStorage.setItem('selectedProject', JSON.stringify(this.selectedProject));
        localStorage.setItem('issues', JSON.stringify(this.issues));
      }
    )

  }

  onDeleteIssue(issueKey: string) {
    this.jiraService.deleteIssue(issueKey)
      .subscribe(
        () => {
          console.log('Ticket Jira supprimé avec succès !');
          this.issues = this.issues.filter(issue => issue.issueKey !== issueKey);
          this.createNotification('success', 'Suppression réussie', 'Le ticket a été supprimé avec succès.');
        },
        (error) => {
          console.error('Erreur lors de la suppression du ticket Jira :', error);
        }
      );
  }

  editIssue(issue: any) {
    this.router.navigateByUrl("/client/editTicketJira/" + issue.issueKey);
  }

  createNotification(type: string, title: string, content: string): void {
    this.notification.create(
      type,
      title,
      content
    );
  }
}
