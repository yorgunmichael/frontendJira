import {Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {JiraService} from "../../../../core/services/jira.service";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzWaveModule} from "ng-zorro-antd/core/wave";
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzInputModule} from "ng-zorro-antd/input";
import {BasicProject} from "../../../../BasicProject";
import {SecurityService} from "../../../../core/services/security.service";
import {NzTableModule} from "ng-zorro-antd/table";

@Component({
  selector: 'app-history-jiraticket',
  standalone: true,
  imports: [CommonModule, NzButtonModule, NzFormModule, NzGridModule, NzInputModule, NzLayoutModule, NzSelectModule, NzWaveModule, ReactiveFormsModule, FormsModule, NzTableModule],
  templateUrl: './history-jiraticket.component.html',
  styleUrls: ['./history-jiraticket.component.css']
})
export class HistoryJiraticketComponent implements OnInit {

  userProjects: BasicProject[] = [];
  selectedTypeIssue!: string;
  historyFormGroup!: FormGroup;
  changelogGroups: any[] = [];
  issueKey!: string;
  issueTypes: string[] = [];
  name_project!: string;
  selectedProjectKey!: string;
  filteredTickets: any[] = [];
  selectedIssueHistory: any[] = [];
  selectedIssueKey!: string;


  constructor(private jiraService: JiraService, private formBuilder: FormBuilder, private securityService: SecurityService) {
  }

  ngOnInit(): void {

    this.historyFormGroup = this.formBuilder.group({

      projectKey: ['', Validators.required],
      issueType: ['', Validators.required],
      issueKey: ['', Validators.required]

    });

    const userName = this.getUserName();
    console.log('email de user ===>', userName);

    if (userName) {
      this.getAllProjectsForUser();
    }

  }


  loadIssueChangelog(): void {
    this.jiraService.getIssueChangelog(this.issueKey).subscribe(
      (data) => {
        this.changelogGroups = data;
      },
      (error) => {
        console.error('Erreur lors du chargement de l\'historique :', error);
      }
    );
  }

  onIssueTypeChange() {

    this.selectedTypeIssue = this.historyFormGroup.controls['issueType'].value;
    const currentProject = this.selectedProjectKey;
    console.log('Current Project ===> ', currentProject);
    this.jiraService.AllJiraTickets(currentProject, this.selectedTypeIssue).subscribe(
      (filteredTickets) => {
        this.filteredTickets = filteredTickets;
        console.log('Updated issueKeys:', filteredTickets);
        this.historyFormGroup.controls['issueKey'].setValue(filteredTickets[0]);
      },
      (error) => {
        console.error('Erreur lors de la récupération des issueKeys:', error);
      }
    );
  }

  onProjectChange() {
    this.selectedProjectKey = this.historyFormGroup.controls['projectKey'].value;

    console.log('selectedProjectKey ===> ', this.selectedProjectKey);

    if (this.selectedProjectKey) {

      if (this.selectedProjectKey === 'TICK') {
        this.issueTypes = ['Bugs', 'Deployment', 'Evolution'];
      } else if (this.selectedProjectKey == 'PROJ') {
        this.issueTypes = ['Bugs', 'Tâche', 'Epic', 'Story'];
      } else if (this.selectedProjectKey === 'ASS') {
        this.issueTypes = ['Bugs', 'Feature', 'Epic'];
      }
      this.historyFormGroup.controls['issueType'].setValue(this.issueTypes[0])

      this.name_project = this.historyFormGroup.controls['projectKey'].value;
      localStorage.setItem('selectedProject', JSON.stringify(this.name_project));
      console.log('project actuel ======> ', this.name_project);
    }
  }

  getUserName(): string | undefined {
    const userName = this.securityService.profile?.name
    console.log('Nouveau nom de l utilisateur ==> ', userName);
    return userName;
  }

  getAllProjectsForUser() {
    const nameofUser = this.getUserName();
    console.log('userName==>', nameofUser)

    if (nameofUser) {
      this.jiraService.getAllProjectsByUser(nameofUser).subscribe(
        namesProject => {
          this.userProjects = namesProject;
          console.log('affichage des noms de projets en cours....')
          console.log('data ::::&&&&', namesProject);
          localStorage.setItem('projects', JSON.stringify(this.userProjects));
        },
        error => {
          console.log(error);
        }
      );
    }
  }


  loadIssueHistory() {
    this.selectedIssueKey = this.historyFormGroup.controls['issueKey'].value;
    if (this.selectedIssueKey) {
      this.jiraService.getIssueChangelog(this.selectedIssueKey).subscribe(
        (issueHistory) => {
          this.selectedIssueHistory = issueHistory;
        },
        (error) => {
          console.error('Erreur lors du chargement de l\'historique du ticket :', error);
        }
      );
    }
  }

  onIssueKeyChange() {
    this.loadIssueHistory();
  }

}
