import {Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {Issue} from "../../../../Issue";
import {JiraService} from "../../../../core/services/jira.service";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzWaveModule} from "ng-zorro-antd/core/wave";
import {Project} from "../../../../Project";
import {AuthentificationService} from "../../../../core/services/authentification.service";
import {SecurityService} from "../../../../core/services/security.service";
import {NzNotificationService} from "ng-zorro-antd/notification";

@Component({
  selector: 'app-edit-ticket',
  standalone: true,
  imports: [CommonModule, NzButtonModule, NzFormModule, NzGridModule, NzInputModule, NzLayoutModule, NzSelectModule, NzWaveModule, ReactiveFormsModule],
  templateUrl: './edit-ticket.component.html',
  styleUrls: ['./edit-ticket.component.css']
})
export class EditTicketComponent implements OnInit {

  issueId!: number
  issue!: Issue
  issueEditFormGroup!: FormGroup
  projects: Project[] = [];
  selectedProject: Project | undefined;
  name_project: string = '';
  picture: File | undefined;
  email: string = '';


  constructor(private fb: FormBuilder, private route: ActivatedRoute, public jiraService: JiraService, private authen: AuthentificationService, private securityService: SecurityService, private notification: NzNotificationService) {
    this.issueId = this.route.snapshot.params['issueId'];
  }

  ngOnInit(): void {

    this.jiraService.getIssue(this.issueId).subscribe(
      (issue: Issue) => {
        this.issue = issue;
        console.log('voici editttttttt===> ',issue)
        this.issueEditFormGroup = this.fb.group({
          selectedProject: [this.issue.nameProject],
          title: [this.issue.title, Validators.required],
          issuetype: [this.issue.typesIssue, Validators.required],
          date_creation: [this.issue.dateCreated],
          description: [this.issue.description, Validators.required],
          status: [this.issue.status],
          priority: [this.issue.priority, Validators.required],
          url: [this.issue.url, Validators.required],
          env: [this.issue.env, Validators.required],
          picture: []
        });
      },
      (error) => {
        console.log(error);
      }
    );

    const userEmail = this.getUserEmail();
    console.log('email de user ===>', userEmail);
    if (userEmail) {
      this.getNameProject();
    }
  }

  onProjectChange() {
    if (this.selectedProject) {
      this.name_project = JSON.parse(JSON.stringify(this.selectedProject));
      console.log("projet====>:", this.name_project);
      console.log('fin de changement de nameproject')
    }
  }

  onFileSelected(event: any) {
    this.picture = event.target.files[0];
    console.log("this.picture ===>", this.picture);
    if (this.picture) {
      console.log("Nom du fichier :", this.picture.name);
    }
  }


  getUserEmail(): string | undefined {
    const userEmail = this.securityService.profile?.email;
    console.log('emmmammamail ==>', userEmail);
    return userEmail;
  }

  getNameProject() {
    const userEmail = this.getUserEmail();

    if(userEmail) {
      this.jiraService.getProjectsByEmail(userEmail).subscribe(
        namesProject => {
          this.projects = namesProject;
          console.log('data ::::', namesProject);
        },
        error => {
          console.log(error);
        }
      );
    }
  }


  handleUpdateIssue() {

    let title = this.issueEditFormGroup.value.title;
    let description = this.issueEditFormGroup.value.description;
    let issuetype = this.issueEditFormGroup.value.issuetype;
    let priority = this.issueEditFormGroup.value.priority;
    let date_creation = this.issueEditFormGroup.value.date_creation;
    let status = this.issueEditFormGroup.value.status;
    let picture = this.picture;
    let url = this.issueEditFormGroup.value.url;
    let selectedProject = this.issueEditFormGroup.value.selectedProject;
    let env = this.issueEditFormGroup.value.env;


    console.log('Les valeurs à maj======+>>> ',this.issueEditFormGroup.value);
    console.log('thissssspicture======+>>> ',this.picture);

    if(picture) {
      this.jiraService.updateIssue(this.issueId, title, description, issuetype, priority, date_creation, status, picture, url, selectedProject, env).subscribe(
        (response: any) => {
          console.log("Informations soumises au backend pour la mise à jour");
          console.log('Mise à jour du ticket: ', response);
          this.createNotification('success');
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  formatDate(date: string): string {
    if (!date) {
      return '';
    }
    return new Date(date).toISOString().split('T')[0];
  }

 createNotification(type: string) {
    this.notification.create(
      type,
      'Succès',
      'Le ticket a été mis à jour avec succès.'
    );
  }
}
