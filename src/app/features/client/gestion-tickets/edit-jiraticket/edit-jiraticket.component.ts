import {Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {BasicProject} from "../../../../BasicProject";
import {ActivatedRoute} from "@angular/router";
import {JiraService} from "../../../../core/services/jira.service";
import {AuthentificationService} from "../../../../core/services/authentification.service";
import {SecurityService} from "../../../../core/services/security.service";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzWaveModule} from "ng-zorro-antd/core/wave";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzInputModule} from "ng-zorro-antd/input";
import {BugsIssue} from "../../../../BugsIssue";
import {DeployIssue} from "../../../../DeployIssue";
import {EvolutionIssue} from "../../../../EvolutionIssue";

@Component({
  selector: 'app-edit-jiraticket',
  standalone: true,
  imports: [CommonModule, NzButtonModule, NzFormModule, NzGridModule, NzInputModule, NzLayoutModule, NzSelectModule, NzWaveModule, ReactiveFormsModule, FormsModule],
  templateUrl: './edit-jiraticket.component.html',
  styleUrls: ['./edit-jiraticket.component.css']
})
export class EditJiraticketComponent implements OnInit {

  issueKey!: string;
  issues!: any;
  issueEditFormGroup!: FormGroup;
  userProjects: BasicProject[] = [];
  selectedProject: BasicProject | undefined;
  name_project: string = '';
  selectedIssueType: string = '';
  picture: File[] = [];

  reporterOptions = [
    {label: 'jordanbrio', value: '64214bba9d6383e32a3388de'},
    {label: 'michaelchacha', value: '712020:22ec5b08-684e-4511-931d-6585fdddb583'},
  ];

  issueLinkedOptions = [
    {label: 'TICK-252', value: 'TICK-252'},
    {label: 'TICK-240', value: 'TICK-240'},
    {label: 'TICK-242', value: 'TICK-242'},
  ];

  levelOptions = [
    {label: 'Mineur', value: '10032'},
    {label: 'Majeur', value: '10033'},
    {label: 'Bloquant', value: '10034'},
  ];

  natureOptions = [
    {label: 'Evolution prise en charge par Qualitadd', value: '10025'},
  ];

  requestFormatOptions = [
    {label: 'Mail', value: '10024'},
  ];

  priorityOptions = [
    {label: 'Highest', value: '10030'},
    {label: 'High', value: '10026'},
    {label: 'Medium', value: '10027'},
    {label: 'Low', value: '10029'},
    {label: 'Lowest', value: '10028'},
  ];

  recognitionEvolutionOptions = [
    {label: 'Bon de commande indépendant', value: '10035'},
    {label: 'Bon de commande dans un lot', value: '10036'},
    {label: 'MeV', value: '10037'},
  ];


  constructor(private fb: FormBuilder, private route: ActivatedRoute, public jiraService: JiraService, private authen: AuthentificationService, private securityService: SecurityService, private notification: NzNotificationService) {
    this.issueKey = this.route.snapshot.params['issueKey'];
    console.log('JE VEUX VOIR @@@  issuekey:', this.issueKey);
  }

  ngOnInit(): void {

    this.jiraService.getIssueJira(this.issueKey).subscribe(
      (issue: any) => {
        this.issues = issue;
        console.log('#########*********voici editttttttt===> ', issue)

        console.log('N@@@@@MEEEEE ===> ', this.issues.reporter);
        const valueUserID = this.reporterOptions.find(option => option.label === this.issues.reporter)?.value;
        const valueIssueLinked = this.issueLinkedOptions.find(option => option.label === this.issues.issueLinked)?.value;
        const valueLevel = this.levelOptions.find(option => option.label === this.issues.level)?.value;
        const valueRequestFormat = this.requestFormatOptions.find(option => option.label === this.issues.requestFormat)?.value;
        const valuePriority = this.priorityOptions.find(option => option.label === this.issues.priority)?.value;
        const valueNature = this.natureOptions.find(option => option.label === this.issues.nature)?.value;
        const valueRecognitionEvolution = this.recognitionEvolutionOptions.find(option => option.label === this.issues.recognitionEvolution)?.value;

        console.log('AAAAAAAAA====> ', this.issues.requestFormat);

        this.issueEditFormGroup = this.fb.group({

          projectKey: [this.issues.projectKey],
          summary: [this.issues.summary, Validators.required],
          description: [this.issues.description, Validators.required],
          issueType: [this.issues.issueType, Validators.required],
          reporter: [valueUserID, Validators.required],
          managerProduct: [valueUserID, Validators.required],
          managerRD: [valueUserID, Validators.required],
          collaborator: [valueUserID, Validators.required],
          environmentUrl: [this.issues.environmentUrl, Validators.required],
          issueLinked: [valueIssueLinked, Validators.required],
          commentRecipeKO: [this.issues.commentRecipeKO, Validators.required],
          dateOfRequest: [this.convertDate(this.issues.dateOfRequest), Validators.required],
          deadlineIntern: [this.convertDate(this.issues.deadlineIntern), Validators.required],
          deadlineCustomer: [this.convertDate(this.issues.deadlineCustomer), Validators.required],
          dateOfRealization: [this.convertDate(this.issues.dateOfRealization), Validators.required],
          dateOfDelivery: [this.convertDate(this.issues.dateOfDelivery), Validators.required],
          comments: [this.issues.comments, Validators.required],
          interestsCollateral: [this.issues.interestsCollateral, Validators.required],
          level: [valueLevel, Validators.required],
          requestFormat: [valueRequestFormat, Validators.required],
          detectedBy: [valueUserID, Validators.required],
          priority: [valuePriority, Validators.required],
          nature: [valueNature, Validators.required],
          recognitionEvolution: [valueRecognitionEvolution, Validators.required],
          estimatePure: [this.issues.estimatePure, Validators.required],
          estimateTotal: [this.issues.estimateTotal, Validators.required],
          pictures: ['', Validators.required]
        });
      },
      (error) => {
        console.log(error);
      }
    );

    const userName = this.getUserName();
    console.log('Name de user ===>', userName);
    if (userName) {
      this.getProjectsForUsers();
    }

    this.selectedIssueType = this.issueEditFormGroup.get('issueType')?.value;
    this.onIssueTypeChange();
  }

  getUserName(): string | undefined {
    const userName = this.securityService.profile?.name
    console.log('Nouveau nom de l utilisateur ==> ', userName);
    return userName;
  }

  getProjectsForUsers() {
    const userNames = this.getUserName();
    console.log('userName==>', userNames)

    if (userNames) {
      this.jiraService.getAllProjectsByUser(userNames).subscribe(
        namesProject => {
          this.userProjects = namesProject;
          console.log('affichage des noms de projets en cours....')
          console.log('data ::::&&&&', namesProject);
          localStorage.setItem('projects', JSON.stringify(this.userProjects));
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  onProjectChange() {
    if (this.selectedProject) {
      this.name_project = JSON.parse(JSON.stringify(this.selectedProject));
      localStorage.setItem('selectedProject', JSON.stringify(this.selectedProject));
      console.log('project actuel ======> ', this.name_project);
    }
  }

  onUpdateIssue() {
    if (this.issueEditFormGroup.valid) {
      const type = this.issueEditFormGroup.value.issueType;
      let dataIssue: any


      if (type === 'Bugs') {
        console.log('Je suis dans Bugs');
        dataIssue = {
          type: this.issueEditFormGroup.value.issueType,
          projectKey: this.issueEditFormGroup.value.projectKey,
          reporter: this.issueEditFormGroup.value.reporter,
          issueType: this.issueEditFormGroup.value.issueType,
          managerProduct: this.issueEditFormGroup.value.managerProduct,
          managerRD: this.issueEditFormGroup.value.managerRD,
          collaborator: this.issueEditFormGroup.value.collaborator,
          environmentUrl: this.issueEditFormGroup.value.environmentUrl,
          summary: this.issueEditFormGroup.value.summary,
          description: this.issueEditFormGroup.value.description,
          issueLinked: this.issueEditFormGroup.value.issueLinked,
          commentRecipeKO: this.issueEditFormGroup.value.commentRecipeKO,
          dateOfRequest: this.issueEditFormGroup.value.dateOfRequest,
          deadlineIntern: this.issueEditFormGroup.value.deadlineIntern,
          deadlineCustomer: this.issueEditFormGroup.value.deadlineCustomer,
          dateOfRealization: this.issueEditFormGroup.value.dateOfRealization,
          dateOfDelivery: this.issueEditFormGroup.value.dateOfDelivery,
          comments: this.issueEditFormGroup.value.comments,
          interestsCollateral: this.issueEditFormGroup.value.interestsCollateral,
          level: this.issueEditFormGroup.value.level,
          requestFormat: this.issueEditFormGroup.value.requestFormat,
          detectedBy: this.issueEditFormGroup.value.detectedBy
        } as BugsIssue;
      } else if (type === 'Deployment') {
        console.log('Je suis dans Déploiement');
        dataIssue = {
          type: this.issueEditFormGroup.value.issueType,
          projectKey: this.issueEditFormGroup.value.projectKey,
          reporter: this.issueEditFormGroup.value.reporter,
          issueType: this.issueEditFormGroup.value.issueType,
          managerProduct: this.issueEditFormGroup.value.managerProduct,
          managerRD: this.issueEditFormGroup.value.managerRD,
          collaborator: this.issueEditFormGroup.value.collaborator,
          environmentUrl: this.issueEditFormGroup.value.environmentUrl,
          summary: this.issueEditFormGroup.value.summary,
          description: this.issueEditFormGroup.value.description,
          issueLinked: this.issueEditFormGroup.value.issueLinked,
          commentRecipeKO: this.issueEditFormGroup.value.commentRecipeKO,
          dateOfRequest: this.issueEditFormGroup.value.dateOfRequest,
          deadlineIntern: this.issueEditFormGroup.value.deadlineIntern,
          deadlineCustomer: this.issueEditFormGroup.value.deadlineCustomer,
          dateOfRealization: this.issueEditFormGroup.value.dateOfRealization,
          dateOfDelivery: this.issueEditFormGroup.value.dateOfDelivery,
          comments: this.issueEditFormGroup.value.comments,
          interestsCollateral: this.issueEditFormGroup.value.interestsCollateral,
          priority: this.issueEditFormGroup.value.priority,
          detectedBy: this.issueEditFormGroup.value.detectedBy,
          nature: this.issueEditFormGroup.value.nature,
          recognitionEvolution: this.issueEditFormGroup.value.recognitionEvolution,
          estimatePure: this.issueEditFormGroup.value.estimatePure,
          estimateTotal: this.issueEditFormGroup.value.estimateTotal
        } as DeployIssue;
      } else if (type === 'Evolution') {
        console.log('Je suis dans Evolution');
        dataIssue = {
          type: this.issueEditFormGroup.value.issueType,
          projectKey: this.issueEditFormGroup.value.projectKey,
          reporter: this.issueEditFormGroup.value.reporter,
          issueType: this.issueEditFormGroup.value.issueType,
          managerProduct: this.issueEditFormGroup.value.managerProduct,
          managerRD: this.issueEditFormGroup.value.managerRD,
          collaborator: this.issueEditFormGroup.value.collaborator,
          environmentUrl: this.issueEditFormGroup.value.environmentUrl,
          summary: this.issueEditFormGroup.value.summary,
          description: this.issueEditFormGroup.value.description,
          issueLinked: this.issueEditFormGroup.value.issueLinked,
          commentRecipeKO: this.issueEditFormGroup.value.commentRecipeKO,
          dateOfRequest: this.issueEditFormGroup.value.dateOfRequest,
          deadlineIntern: this.issueEditFormGroup.value.deadlineIntern,
          deadlineCustomer: this.issueEditFormGroup.value.deadlineCustomer,
          dateOfRealization: this.issueEditFormGroup.value.dateOfRealization,
          dateOfDelivery: this.issueEditFormGroup.value.dateOfDelivery,
          comments: this.issueEditFormGroup.value.comments,
          interestsCollateral: this.issueEditFormGroup.value.interestsCollateral,
          priority: this.issueEditFormGroup.value.priority,
          nature: this.issueEditFormGroup.value.nature,
          estimatePure: this.issueEditFormGroup.value.estimatePure,
          estimateTotal: this.issueEditFormGroup.value.estimateTotal,
          recognitionEvolution: this.issueEditFormGroup.value.recognitionEvolution,
          requestFormat: this.issueEditFormGroup.value.requestFormat
        } as EvolutionIssue;
      }
      console.log("Informations soumises au backend pour la mise à jour");
      this.jiraService.updateJiraIssue(this.issueKey, dataIssue, type, this.picture).subscribe(
        (response: any) => {
          console.log("Informations soumises au backend pour la mise à jour");
          this.createNotification('success');
        },
        (error) => {
          console.log(error);
        }
      );
      this.issueEditFormGroup.reset();
    }
  }

  createNotification(type: string) {
    this.notification.create(
      type,
      'Succès',
      'Le ticket a été mis à jour avec succès.'
    );
  }


  onIssueTypeChange() {
    this.selectedIssueType = this.issueEditFormGroup.get('issueType')?.value;
    console.log('Voici le type d issue sélectionné', this.selectedIssueType);

    if (this.selectedIssueType === 'Bugs') {
      this.issueEditFormGroup.get('priority')?.clearValidators();
      this.issueEditFormGroup.get('priority')?.updateValueAndValidity();
      this.issueEditFormGroup.get('nature')?.clearValidators();
      this.issueEditFormGroup.get('nature')?.updateValueAndValidity();
      this.issueEditFormGroup.get('recognitionEvolution')?.clearValidators();
      this.issueEditFormGroup.get('recognitionEvolution')?.updateValueAndValidity();
      this.issueEditFormGroup.get('estimatePure')?.clearValidators();
      this.issueEditFormGroup.get('estimatePure')?.updateValueAndValidity();
      this.issueEditFormGroup.get('estimateTotal')?.clearValidators();
      this.issueEditFormGroup.get('estimateTotal')?.updateValueAndValidity();
      this.issueEditFormGroup.get('requestFormat')?.clearValidators();
      this.issueEditFormGroup.get('requestFormat')?.updateValueAndValidity();
    } else if (this.selectedIssueType === 'Deployment') {
      this.issueEditFormGroup.get('level')?.clearValidators();
      this.issueEditFormGroup.get('level')?.updateValueAndValidity();
      this.issueEditFormGroup.get('requestFormat')?.clearValidators();
      this.issueEditFormGroup.get('requestFormat')?.updateValueAndValidity();
    } else if (this.selectedIssueType === 'Evolution') {
      this.issueEditFormGroup.get('level')?.clearValidators();
      this.issueEditFormGroup.get('level')?.updateValueAndValidity();
      this.issueEditFormGroup.get('detectedBy')?.clearValidators();
      this.issueEditFormGroup.get('detectedBy')?.updateValueAndValidity();
    }
  }

  convertDate(date: string): string {
    return new Date(date).toISOString().split('T')[0];
  }

  onPictureSelected(event: any) {
    const selectedFiles: FileList = event.target.files;
    for (let i = 0; i < selectedFiles.length; i++) {
      const pictureSelected: File | null = selectedFiles.item(i);

      if (pictureSelected) {
        this.picture.push(pictureSelected);
        console.log("Nom du fichier :", pictureSelected.name);
      }
    }

    if (this.picture) {
      console.log("Images sélectionnées :", this.picture);
    }
  }
}
