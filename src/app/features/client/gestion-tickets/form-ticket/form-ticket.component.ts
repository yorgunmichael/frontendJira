import {Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NzFormModule} from "ng-zorro-antd/form";
import {NzInputModule} from "ng-zorro-antd/input";
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, ValidationErrors, Validators} from "@angular/forms";
import {NzButtonModule} from "ng-zorro-antd/button";
import {Project} from "../../../../Project";
import {JiraService} from "../../../../core/services/jira.service";
import {Router, RouterLink} from "@angular/router";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzCardModule} from "ng-zorro-antd/card";
import {MainLayoutComponent} from "../../../../layout/main-layout/main-layout.component";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {AuthentificationService} from "../../../../core/services/authentification.service";
import {SecurityService} from "../../../../core/services/security.service";


@Component({
  selector: 'app-form-ticket',
  standalone: true,
  imports: [CommonModule, NzFormModule, NzInputModule, ReactiveFormsModule, NzButtonModule, NzSelectModule, FormsModule, NzDatePickerModule, NzUploadModule, NzIconModule, NzCardModule, MainLayoutComponent, RouterLink, NzLayoutModule, NzTypographyModule],
  templateUrl: './form-ticket.component.html',
  styleUrls: ['./form-ticket.component.css']
})
export class FormTicketComponent implements OnInit {

  name_project: string = '';
  picture: File | undefined;
  projects: Project[] = [];
  selectedProject: Project | undefined;
  issueFormGroup!: FormGroup


  constructor(public jiraService: JiraService, private router: Router, private authenService: AuthentificationService, private fb: FormBuilder, private securityService: SecurityService) {
  }

  ngOnInit(): void {

    this.issueFormGroup = this.fb.group({
      selectedProject: this.fb.control(null),
      title: this.fb.control(null, Validators.required),
      issuetype: this.fb.control(null, Validators.required),
      date_creation: this.fb.control(null),
      description: this.fb.control(null, Validators.required),
      status: this.fb.control(null),
      priority: this.fb.control('', Validators.required),
      url: this.fb.control(null, Validators.required),
      env: this.fb.control(null, Validators.required),
      picture: this.fb.control('')
    })


    // this.getUserByEmail();
    // this.getUserEmail();
    // this.getNameProject(this.getUserEmail());
    const userEmail = this.getUserEmail();
    console.log('email de user ===>', userEmail)
    if (userEmail) {
      this.getNameProject(); // Utilisez userEmail au lieu de this.email
    }
    console.log("selectedProject initial value:", this.selectedProject);
    console.log("projects initial value:", this.projects);
  }

  getUserEmail(): string | undefined {
    const userEmail = this.securityService.profile?.email;
    return userEmail;
  }



  onProjectChange() {
    if (this.selectedProject) {
      this.name_project = JSON.parse(JSON.stringify(this.selectedProject));
      console.log("projet====>:", this.name_project);
      console.log('fin de changement de nameproject')
    }
  }

  onFileSelected(event: any) {
    this.picture = event.target.files[0];
    console.log("this.picture ===>", this.picture);
    if (this.picture) {
      console.log("Nom du fichier :", this.picture.name);
    }
  }


  // getNameProject(email: string) {
  //   this.jiraService.getProjectsByEmail(email).subscribe(
  //     namesProject => {
  //       this.projects = namesProject;
  //       console.log('data ::::', namesProject);
  //     },
  //     error => {
  //       console.log(error);
  //     }
  //   );
  // }

  getNameProject() {
    const userEmail = this.getUserEmail(); // Utilisez la méthode getUserEmail() pour obtenir l'e-mail

    if(userEmail) {
    this.jiraService.getProjectsByEmail(userEmail).subscribe( // Utilisez userEmail au lieu de email
      namesProject => {
        this.projects = namesProject;
        console.log('data ::::', namesProject);
      },
      error => {
        console.log(error);
      }
    );
    }
  }


  // getUserByEmail() {
  //   this.authenService.getUserName().subscribe(
  //     (email: string) => {
  //       this.email = email;
  //       localStorage.setItem('username', email);
  //       console.log('eemmmmmail: ' + email);
  //     }
  //   );
  // }

  handleAddIssue() {

    let title = this.issueFormGroup.value.title;
    let description = this.issueFormGroup.value.description;
    let issuetype = this.issueFormGroup.value.issuetype;
    let priority = this.issueFormGroup.value.priority;
    let date_creation = this.issueFormGroup.value.date_creation;
    let status = this.issueFormGroup.value.status;
    let picture = this.picture;
    let url = this.issueFormGroup.value.url;
    let selectedProject = this.issueFormGroup.value.selectedProject;
    let env = this.issueFormGroup.value.env;

    console.log(this.issueFormGroup.value);
    if (picture) {
      this.jiraService
        .createTicket(title, description, issuetype, priority, date_creation, status, picture, url, selectedProject, env).subscribe((response: any) => {
        console.log("reponseeeeeeeee ", response);
        console.log("picturecomponent ", this.picture);
      });
    }
    this.router.navigate(['/client/formConfirmation']);
  }

}
