import {Component, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {JiraService} from "../../../../core/services/jira.service";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzFormModule} from "ng-zorro-antd/form";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {NzSelectModule} from "ng-zorro-antd/select";
import {NzNotificationService} from "ng-zorro-antd/notification";
import {NzDatePickerModule} from "ng-zorro-antd/date-picker";
import {NzUploadModule} from "ng-zorro-antd/upload";
import {NzIconModule} from "ng-zorro-antd/icon";
import {NzCardModule} from "ng-zorro-antd/card";
import {MainLayoutComponent} from "../../../../layout/main-layout/main-layout.component";
import {RouterLink} from "@angular/router";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {BasicProject} from "../../../../BasicProject";
import {SecurityService} from "../../../../core/services/security.service";
import {BugsIssue} from "../../../../BugsIssue";
import {DeployIssue} from "../../../../DeployIssue";
import {EvolutionIssue} from "../../../../EvolutionIssue";

@Component({
  selector: 'app-forms-jiraticket',
  standalone: true,
  imports: [CommonModule, NzFormModule, NzInputModule, ReactiveFormsModule, NzButtonModule, NzSelectModule, FormsModule, NzDatePickerModule, NzUploadModule, NzIconModule, NzCardModule, MainLayoutComponent, RouterLink, NzLayoutModule, NzTypographyModule],
  templateUrl: './forms-jiraticket.component.html',
  styleUrls: ['./forms-jiraticket.component.css']
})
export class FormsJiraticketComponent implements OnInit {
  issueForm!: FormGroup;
  userProjects: BasicProject[] = [];
  selectedProject: BasicProject | undefined;
  name_project: string = '';
  selectedIssueType: string = '';
  picture: File[] = [];


  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, public jiraService: JiraService, private notification: NzNotificationService) {
  }

  ngOnInit(): void {
    this.issueForm = this.formBuilder.group({

      projectKey: ['', Validators.required],
      summary: ['', Validators.required],
      description: ['', Validators.required],
      issueType: ['', Validators.required],
      reporter: ['', Validators.required],
      managerProduct: ['', Validators.required],
      managerRD: ['', Validators.required],
      collaborator: ['', Validators.required],
      environmentUrl: ['', Validators.required],
      issueLinked: ['', Validators.required],
      commentRecipeKO: ['', Validators.required],
      dateOfRequest: ['', Validators.required],
      deadlineIntern: ['', Validators.required],
      deadlineCustomer: ['', Validators.required],
      dateOfRealization: ['', Validators.required],
      dateOfDelivery: ['', Validators.required],
      comments: ['', Validators.required],
      interestsCollateral: ['', Validators.required],
      level: ['', Validators.required],
      requestFormat: ['', Validators.required],
      detectedBy: ['', Validators.required],
      priority: ['', Validators.required],
      nature: ['', Validators.required],
      recognitionEvolution: ['', Validators.required],
      estimatePure: ['', Validators.required],
      estimateTotal: ['', Validators.required],
      pictures: ['', Validators.required]

    });

    const userName = this.getUserName();
    console.log('email de user ===>', userName);

    if (userName) {
      this.getProjectsForUser();
    }

    this.selectedIssueType = this.issueForm.get('issueType')?.value;
    this.onIssueTypeChange();
  }


  createIssueJira() {


    if (this.issueForm.valid) {

      const type = this.issueForm.value.issueType;

      let dataIssue: any

      if (type === 'Bugs') {
        console.log('Je suis dans Bugs');
        dataIssue = {
          type: this.issueForm.value.issueType,
          projectKey: this.issueForm.value.projectKey,
          reporter: this.issueForm.value.reporter,
          issueType: this.issueForm.value.issueType,
          managerProduct: this.issueForm.value.managerProduct,
          managerRD: this.issueForm.value.managerRD,
          collaborator: this.issueForm.value.collaborator,
          environmentUrl: this.issueForm.value.environmentUrl,
          summary: this.issueForm.value.summary,
          description: this.issueForm.value.description,
          issueLinked: this.issueForm.value.issueLinked,
          commentRecipeKO: this.issueForm.value.commentRecipeKO,
          dateOfRequest: this.issueForm.value.dateOfRequest,
          deadlineIntern: this.issueForm.value.deadlineIntern,
          deadlineCustomer: this.issueForm.value.deadlineCustomer,
          dateOfRealization: this.issueForm.value.dateOfRealization,
          dateOfDelivery: this.issueForm.value.dateOfDelivery,
          comments: this.issueForm.value.comments,
          interestsCollateral: this.issueForm.value.interestsCollateral,
          level: this.issueForm.value.level,
          requestFormat: this.issueForm.value.requestFormat,
          detectedBy: this.issueForm.value.detectedBy,
        } as BugsIssue;
      } else if (type === 'Deployment') {
        console.log('Je suis dans Déploiement');
        dataIssue = {
          type: this.issueForm.value.issueType,
          projectKey: this.issueForm.value.projectKey,
          reporter: this.issueForm.value.reporter,
          issueType: this.issueForm.value.issueType,
          managerProduct: this.issueForm.value.managerProduct,
          managerRD: this.issueForm.value.managerRD,
          collaborator: this.issueForm.value.collaborator,
          environmentUrl: this.issueForm.value.environmentUrl,
          summary: this.issueForm.value.summary,
          description: this.issueForm.value.description,
          issueLinked: this.issueForm.value.issueLinked,
          commentRecipeKO: this.issueForm.value.commentRecipeKO,
          dateOfRequest: this.issueForm.value.dateOfRequest,
          deadlineIntern: this.issueForm.value.deadlineIntern,
          deadlineCustomer: this.issueForm.value.deadlineCustomer,
          dateOfRealization: this.issueForm.value.dateOfRealization,
          dateOfDelivery: this.issueForm.value.dateOfDelivery,
          comments: this.issueForm.value.comments,
          interestsCollateral: this.issueForm.value.interestsCollateral,
          priority: this.issueForm.value.priority,
          detectedBy: this.issueForm.value.detectedBy,
          nature: this.issueForm.value.nature,
          recognitionEvolution: this.issueForm.value.recognitionEvolution,
          estimatePure: this.issueForm.value.estimatePure,
          estimateTotal: this.issueForm.value.estimateTotal
        } as DeployIssue;
      } else if (type === 'Evolution') {
        console.log('Je suis dans Evolution');
        dataIssue = {
          type: this.issueForm.value.issueType,
          projectKey: this.issueForm.value.projectKey,
          reporter: this.issueForm.value.reporter,
          issueType: this.issueForm.value.issueType,
          managerProduct: this.issueForm.value.managerProduct,
          managerRD: this.issueForm.value.managerRD,
          collaborator: this.issueForm.value.collaborator,
          environmentUrl: this.issueForm.value.environmentUrl,
          summary: this.issueForm.value.summary,
          description: this.issueForm.value.description,
          issueLinked: this.issueForm.value.issueLinked,
          commentRecipeKO: this.issueForm.value.commentRecipeKO,
          dateOfRequest: this.issueForm.value.dateOfRequest,
          deadlineIntern: this.issueForm.value.deadlineIntern,
          deadlineCustomer: this.issueForm.value.deadlineCustomer,
          dateOfRealization: this.issueForm.value.dateOfRealization,
          dateOfDelivery: this.issueForm.value.dateOfDelivery,
          comments: this.issueForm.value.comments,
          interestsCollateral: this.issueForm.value.interestsCollateral,
          priority: this.issueForm.value.priority,
          nature: this.issueForm.value.nature,
          estimatePure: this.issueForm.value.estimatePure,
          estimateTotal: this.issueForm.value.estimateTotal,
          recognitionEvolution: this.issueForm.value.recognitionEvolution,
          requestFormat: this.issueForm.value.requestFormat
        } as EvolutionIssue;
      }
      console.log("Les information sur Picture ==> ", this.issueForm.value);
      console.log('issueData:====>', dataIssue)
      this.jiraService.createIssue(dataIssue, type, this.picture).subscribe(
        (issueKey: string) => {
          console.log("Nouvelle issue créée avec la clé : ", issueKey);
          this.createNotification('success');
        },
        (error: any) => {
          console.error("Erreur lors de la création de l'issue : ", error);
        }
      );

      this.issueForm.reset();
    }
  }


  createNotification(type: string) {
    this.notification.create(
      type,
      'Succès',
      'Le ticket a été créé avec succès.'
    );
  }


  onIssueTypeChange() {
    this.selectedIssueType = this.issueForm.get('issueType')?.value;
    console.log('Voici le type d issue sélectionné', this.selectedIssueType);

    if (this.selectedIssueType === 'Bugs') {
      this.issueForm.get('priority')?.clearValidators();
      this.issueForm.get('priority')?.updateValueAndValidity();
      this.issueForm.get('nature')?.clearValidators();
      this.issueForm.get('nature')?.updateValueAndValidity();
      this.issueForm.get('recognitionEvolution')?.clearValidators();
      this.issueForm.get('recognitionEvolution')?.updateValueAndValidity();
      this.issueForm.get('estimatePure')?.clearValidators();
      this.issueForm.get('estimatePure')?.updateValueAndValidity();
      this.issueForm.get('estimateTotal')?.clearValidators();
      this.issueForm.get('estimateTotal')?.updateValueAndValidity();
      this.issueForm.get('requestFormat')?.clearValidators();
      this.issueForm.get('requestFormat')?.updateValueAndValidity();
    } else if (this.selectedIssueType === 'Deployment') {
      this.issueForm.get('level')?.clearValidators();
      this.issueForm.get('level')?.updateValueAndValidity();
      this.issueForm.get('requestFormat')?.clearValidators();
      this.issueForm.get('requestFormat')?.updateValueAndValidity();
    } else if (this.selectedIssueType === 'Evolution') {
      this.issueForm.get('level')?.clearValidators();
      this.issueForm.get('level')?.updateValueAndValidity();
      this.issueForm.get('detectedBy')?.clearValidators();
      this.issueForm.get('detectedBy')?.updateValueAndValidity();
    }

  }

  onProjectChange() {
    if (this.issueForm.controls['projectKey'].value) {
      this.name_project = this.issueForm.controls['projectKey'].value;
      localStorage.setItem('selectedProject', JSON.stringify(this.selectedProject));
      console.log('project actuel ======> ', this.name_project);
    }
  }

  getUserName(): string | undefined {
    const userName = this.securityService.profile?.name
    console.log('Nouveau nom de l utilisateur ==> ', userName);
    return userName;
  }

  getProjectsForUser() {
    const userName = this.getUserName();
    console.log('userName==>', userName)

    if (userName) {
      this.jiraService.getAllProjectsByUser(userName).subscribe(
        namesProject => {
          this.userProjects = namesProject;
          console.log('affichage des noms de projets en cours....')
          console.log('data ::::&&&&', namesProject);
          localStorage.setItem('projects', JSON.stringify(this.userProjects));
        },
        error => {
          console.log(error);
        }
      );
    }
  }

  onFileSelected(event: any) {
    const selectedFiles: FileList = event.target.files;
    for (let i = 0; i < selectedFiles.length; i++) {
      const pictureSelected: File | null = selectedFiles.item(i);

      if (pictureSelected) {
        this.picture.push(pictureSelected);
        console.log("Nom du fichier :", pictureSelected.name);
      }
    }

    if (this.picture) {
      console.log("Images sélectionnées :", this.picture);
    }
  }

}
