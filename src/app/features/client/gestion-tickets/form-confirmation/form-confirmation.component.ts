import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NzAlertModule} from "ng-zorro-antd/alert";
import {NzCardModule} from "ng-zorro-antd/card";
import { registerLocaleData } from '@angular/common';
import zh from '@angular/common/locales/zh';
import {NzLayoutModule} from "ng-zorro-antd/layout";
import {MainLayoutComponent} from "../../../../layout/main-layout/main-layout.component";
import {RouterLink} from "@angular/router"; registerLocaleData(zh);

@Component({
  selector: 'app-form-confirmation',
  standalone: true,
  imports: [CommonModule, NzAlertModule, NzCardModule, NzLayoutModule, MainLayoutComponent, RouterLink],
  templateUrl: './form-confirmation.component.html',
  styleUrls: ['./form-confirmation.component.css']
})
export class FormConfirmationComponent {

}
